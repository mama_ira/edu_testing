import Profile from './Profile';
class MainPage{
    dropdown(){
        return cy.get('.dropdown-toggle');
    }
    goPersonalArea(){
        return cy.get('a').contains('Личный кабинет');
    }
    goProfilePage(){
        return new Profile();
    }
}
export default MainPage