import MainPage from './MainPage';

class Registration {
   // constructor() {
     //   this.userPassword = '12345333333333333';
     //   this.userName = Math.round(Math.random() * 1000).toString();
     //   this.userEmail = `${Math.round(Math.random() * 1000)}@gmail.ru`;
     //   this.userEmailTiLowerCase = this.userEmail.toLowerCase();
   // }
    registrationAll(a,b,c) {
        return (
            cy.get('input[name="name"]').type(a),
                cy.get('input[name="email"]').type(b),
                cy.get('input[name="password"]').last().type(c),
                cy.get('input[name="act_register_now"]').click()
        )
    }
    goToMainPage() {
        return new MainPage();
    }
}

export default Registration;
