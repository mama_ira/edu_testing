import Registration from '../PageObject/Registration';
describe('Test buger', () => {
    const URL = 'http://users.bugred.ru/\n';
    beforeEach(() => {
        cy.fixture('users').then(function (data) {
            this.data = data;
        });
        cy.visit(URL);
        cy.get(':nth-child(2) > a > span').click();
    });
    it('Test buger reqister', function () {
        const password = '12345333333333333';
        const name = Math.round(Math.random() * 1000).toString();
        const email = `${Math.round(Math.random() * 1000)}@gmail.ru`;
        new Registration().registrationAll(name, email, password);
        new Registration().goToMainPage().dropdown().click();
        new Registration().goToMainPage().goPersonalArea().click();
        new Registration().goToMainPage().goProfilePage().checkUserEmail()
            .should('have.text', email);
        new Registration().goToMainPage().goProfilePage().checkUrl()
            .should('eq', 'http://users.bugred.ru/user/profile/index.html');

        cy.get('.dropdown-toggle').click();
        cy.get('.dropdown-menu > :nth-child(3) > a').click();
        cy.get(':nth-child(4) > td > .form-control').type(email);
        cy.get(':nth-child(1) > .btn').click();
        cy.get(':nth-child(7) > .btn').click();

    });


    it('Test buger authorization', function () {
       cy.authorization(this.data.userEmail1, this.data.userPassword1);
    new Registration().goToMainPage().dropdown().click();
    new Registration().goToMainPage().goPersonalArea().click();
        const userEmailTiLowerCase = this.data.userEmail1.toLowerCase();
    new Registration().goToMainPage().goProfilePage().checkUserEmail()
        .should('have.text', userEmailTiLowerCase);
    new Registration().goToMainPage().goProfilePage().checkUrl()
        .should('eq', 'http://users.bugred.ru/user/profile/index.html');

    });
});


